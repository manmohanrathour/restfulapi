/* eslint-disable no-useless-escape */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
const path = require('path');
const pool = require('./src/utils/config');

function getPath(fileName) {
  return path.resolve(`${fileName}`);
}

async function dropTable() {
  const query = `DROP TABLE IF EXISTS employee,title;`;
  await pool.query(query);
}

async function createEmployeeTable() {
  try {
    const filePath = getPath('./src/csvData/employee.csv');
    const query = `CREATE TABLE IF NOT EXISTS Employee (
    EMPLOYEE_ID INT PRIMARY KEY,
    FIRST_NAME VARCHAR(25),
    LAST_NAME VARCHAR(25),
    SALARY INT,                                            
    JOINING_DATE DATE,                                       
    DEPARTMENT VARCHAR(25)
   );`;

    await pool.query(query);
    await pool.query(
      `\copy employee from '${filePath}' with delimiter ',' csv header`
    );
  } catch (e) {
    console.log('SQL Exception');
    console.log(e);
  }
}

async function createTitleTbale() {
  try {
    const filePath = getPath('./src/csvData/title.csv');

    const query = `CREATE TABLE IF NOT EXISTS Title (
    EMPLOYEE_REF_ID INT,
    EMPLOYEE_TITLE VARCHAR(25),
    AFFECTED_FROM DATE,
    FOREIGN KEY (EMPLOYEE_REF_ID)
    REFERENCES EMPLOYEE(EMPLOYEE_ID)
    ON DELETE CASCADE
  );`;
    await pool.query(query);
    await pool.query(
      `\copy title from '${filePath}' with delimiter ',' csv header`
    );
  } catch (error) {
    console.log('SQL Exception');
    console.log(error);
  }
}

async function seed() {
  //await pool.connect();
  await dropTable();
  await createEmployeeTable();
  await createTitleTbale();
  await pool.end();
  console.log('seeding done');
}

seed();
