/* eslint-disable prettier/prettier */
const Joi = require('@hapi/joi');
const Extension = require('joi-date-extensions');

const date = Joi.extend(Extension);

// validation  for employee table
function empValidationID(data) {
  const schema = {
    employee_id: Joi.number()
      .integer()
      .required()
  };
  return Joi.validate(data, schema);
}

function empValidation(data) {
  const schema = {
    employee_id: Joi.number()
      .integer()
      .required(),
    first_name: Joi.string()
      .max(25, 'utf8')
      .alphanum()
      .required(),
    last_name: Joi.string()
      .max(25, 'utf8')
      .required(),
    salary: Joi.number()
      .integer()
      .required(),
    department: Joi.string()
      .max(25, 'utf8')
      .required(),
    joining_date: date
      .date()
      .format('YYYY-MM-DD')
      .required()
  };
  return Joi.validate(data, schema);
}

function titleValidationID(data) {
  const schema = {
    employee_ref_id: Joi.number()
      .integer()
      .required()
  };
  return Joi.validate(data, schema);
}

function titleValidation(titleData) {
  const schema = {
    employee_ref_id: Joi.number()
      .integer()
      .required(),
    employee_title: Joi.string()
      .max(25, 'utf8')
      .required(),
    affected_from: date
      .date()
      .format('YYYY-MM-DD')
      .required()
  };
  return Joi.validate(titleData, schema);
}

module.exports = {
  empValidationID,
  empValidation,
  titleValidationID,
  titleValidation
};
