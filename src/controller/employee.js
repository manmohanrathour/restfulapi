const {
  getEmployeeDataByID,
  getEmployeeData,
  deleteEmployee,
  postEmployee,
  putEmployee
} = require('../models/employee');

const { empValidationID, empValidation } = require('../utils/validation.js');
// onst logger = require('../middleware/logging');

// eslint-disable-next-line consistent-return
function getEmpByIDRoutes(req, res, next) {
  const empid = req.params.employee_id;

  const { error } = empValidationID(req.params);
  if (error) {
    next(error.details[0]);
  }

  getEmployeeDataByID(empid)
    .then(emp => {
      //
      if (emp.rowCount === 0) {
        next({
          message: 'id is not available ',
          status: 404
        });
      } else {
        res.send(emp.rows);
      }
    })
    .catch(err => res.send(err.message, err.status));
}

function getEmployeeDataRoutes(req, res, next) {
  getEmployeeData()
    .then(emp => {
      if (emp.count === 0) {
        next({
          message: 'there is no data in the table',
          status: 404
        });
      } else {
        res.send(emp.rows);
      }
    })
    .catch(err => res.send(err.message, err.status));
}

// eslint-disable-next-line consistent-return
function deleteEmployeeByIDRoutes(req, res, next) {
  const { error } = empValidationID(req.params);
  if (error) {
    next(error.details[0]);
  }

  const empid = req.params.employee_id;
  deleteEmployee(empid)
    .then(emp => {
      if (emp.rowCount === 0) {
        next({
          message: 'unable to delete id is not available ',
          status: 404
        });
      } else {
        res.send(`deleted id:${empid}`);
      }
    })
    .catch(err => res.send(err.message, err.status));
}

// eslint-disable-next-line consistent-return
function postEmployeeRoutes(req, res, next) {
  const { error } = empValidation(req.body);
  if (error) {
    next(error.details[0]);
  }
  const values = Object.values(req.body);

  postEmployee(values)
    .then(emp => {
      if (emp.rows) {
        res.send(emp.rows);
      } else {
        res.send(emp.detail);
      }
    })
    .catch(err => res.send(err.message, err.status));
}
// update
// eslint-disable-next-line consistent-return
function putEmployeeRoutes(req, res, next) {
  const result = empValidationID(req.params).error;
  if (result.error) {
    next(result.error.details[0]);
  }
  const { error } = empValidation(req.body);

  if (error) {
    next(error.details[0]);
  }
  const empid = req.params.employee_id;
  const values = Object.values(req.body);
  putEmployee(values, empid)
    .then(emp => {
      if (emp.rowCount === 0) {
        next({
          message: 'unable to process.....Id not found',
          status: 404
        });
      } else if (emp.rows) {
        res.send(emp.rows);
      } else {
        res.send(emp.detail);
      }
    })
    .catch(err => res.send(err.message, err.status));
}

module.exports = {
  getEmpByIDRoutes,
  getEmployeeDataRoutes,
  deleteEmployeeByIDRoutes,
  postEmployeeRoutes,
  putEmployeeRoutes
};
