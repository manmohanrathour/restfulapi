/* eslint-disable consistent-return */
const {
  getEmployeeTitle,
  getEmployeeTitleID,
  postEmployeeTitle,
  deleteEmployeeTitle,
  putEmployeeTitle
} = require('../models/title');

const {
  titleValidationID,
  titleValidation
} = require('../utils/validation.js');

// eslint-disable-next-line consistent-return
function getEmpTitleByIDRoutes(req, res, next) {
  const empid = req.params.employee_ref_id;
  const { error } = titleValidationID(req.params);
  if (error) {
    next(error.details[0]);
  }
  getEmployeeTitleID(empid).then(emp => {
    //
    if (emp.rowCount === 0) {
      next({
        message: 'id is not available ',
        status: 404
      });
    } else {
      res.send(emp.rows);
    }
  });
}

function getEmpTitleDataRoutes(req, res, next) {
  getEmployeeTitle().then(emp => {
    if (emp.count === 0) {
      next({
        message: 'there is no data in the table',
        status: 404
      });
    } else {
      res.send(emp.rows);
    }
  });
}

function deleteEmpTitleByIDRoutes(req, res, next) {
  const { error } = titleValidationID(req.params);
  if (error) {
    next(error.details[0]);
  }
  const empid = req.params.employee_ref_id;
  deleteEmployeeTitle(empid).then(emp => {
    if (emp.rowCount === 0) {
      next({
        message: 'unable to delete id is not available ',
        status: 404
      });
    } else {
      res.send(`deleted id:${empid}`);
    }
  });
}

function postEmpTitleRoutes(req, res, next) {
  const { error } = titleValidation(req.body);
  if (error) {
    next(error.details[0]);
  }
  const values = Object.values(req.body);
  postEmployeeTitle(values).then(emp => {
    if (emp.rows) {
      res.send(emp.rows);
    } else {
      res.send(emp.detail);
    }
  });
}
// eslint-disable-next-line consistent-return
function putEmpTitleRoutes(req, res, next) {
  const result = titleValidationID(req.params).error;
  if (result.error) {
    next(result.error.details[0]);
  }
  const { error } = titleValidation(req.body);
  if (error) {
    next(error.details[0]);
  }
  const empid = req.params.employee_ref_id;
  const values = Object.values(req.body);
  putEmployeeTitle(values, empid).then(emp => {
    if (emp.rowCount === 0) {
      next({
        message: 'unable to process.....Id not found',
        status: 404
      });
    } else if (emp.rows) {
      res.send(emp.rows);
    } else {
      res.send(emp.detail);
    }
  });
}

module.exports = {
  getEmpTitleByIDRoutes,
  getEmpTitleDataRoutes,
  postEmpTitleRoutes,
  deleteEmpTitleByIDRoutes,
  putEmpTitleRoutes
};
