const pool = require('../utils/config.js');

async function getEmployeeData() {
  try {
    const query = `select * from employee`;
    return await pool.query(query);
  } catch (error) {
    return error;
  }
}

async function getEmployeeDataByID(id) {
  try {
    const query = `select * from employee where employee_id=$1`;
    return await pool.query(query, [id]);
  } catch (error) {
    return error;
  }
}

async function deleteEmployee(id) {
  try {
    const query = 'delete from employee where employee_id=$1';
    return await pool.query(query, [id]);
  } catch (error) {
    return error;
  }
}

async function putEmployee(values, empid) {
  try {
    const query =
      'update employee set (employee_id,first_name,last_name,salary,joining_date,department ) = ($1,$2,$3,$4,$5,$6) where employee_id = $7 returning *';
    return await pool.query(query, [...values, empid]);
  } catch (error) {
    return error;
  }
}

async function postEmployee(employeeData) {
  try {
    const query = 'insert into employee values($1,$2,$3,$4,$5,$6) RETURNING *';
    return await pool.query(query, employeeData);
  } catch (error) {
    return error;
  }
}

module.exports = {
  getEmployeeData,
  getEmployeeDataByID,
  postEmployee,
  deleteEmployee,
  putEmployee
};
