const pool = require('../utils/config.js');

async function getEmployeeTitle() {
  try {
    return await pool.query(`select * from title`);
  } catch (error) {
    return error;
  }
}

async function getEmployeeTitleID(id) {
  try {
    const query = `select * from title where employee_ref_id=$1`;
    return await pool.query(query, [id]);
  } catch (error) {
    return error;
  }
}

async function deleteEmployeeTitle(id) {
  try {
    const query = `delete from title where employee_ref_id=$1`;
    return await pool.query(query, [id]);
  } catch (error) {
    return error;
  }
}

async function putEmployeeTitle(values, empid) {
  try {
    const query =
      'update title set (employee_ref_id,employee_title,affected_from ) = ($1,$2) where employee_ref_id = $4 returning *';
    return await pool.query(query, [...values, empid]);
  } catch (error) {
    return error;
  }
}

async function postEmployeeTitle(employeeData) {
  try {
    const query = `insert into title values($1,$2,$3) RETURNING *`;
    return pool.query(query, employeeData);
  } catch (error) {
    return error;
  }
}

module.exports = {
  getEmployeeTitle,
  getEmployeeTitleID,
  postEmployeeTitle,
  putEmployeeTitle,
  deleteEmployeeTitle
};
