/* eslint-disable no-console */
const express = require('express');

const {
  getEmpTitleByIDRoutes,
  getEmpTitleDataRoutes,
  postEmpTitleRoutes,
  deleteEmpTitleByIDRoutes,
  putEmpTitleRoutes
} = require('../controller/title.js');

const router = express.Router();
router.use(express.json());

router.get('/', getEmpTitleDataRoutes);
router.get('/:employee_ref_id', getEmpTitleByIDRoutes);
router.post('/', postEmpTitleRoutes);
router.put('/:employee_ref_id', putEmpTitleRoutes);
router.delete('/:employee_ref_id', deleteEmpTitleByIDRoutes);

module.exports = router;
