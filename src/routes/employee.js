/* eslint-disable no-console */
const express = require('express');

const {
  getEmpByIDRoutes,
  getEmployeeDataRoutes,
  postEmployeeRoutes,
  putEmployeeRoutes,
  deleteEmployeeByIDRoutes
} = require('../controller/employee');

const router = express.Router();
router.use(express.json());

router.get('/', getEmployeeDataRoutes);
router.get('/:employee_id', getEmpByIDRoutes);
router.post('/', postEmployeeRoutes);
router.put('/:employee_id', putEmployeeRoutes);
router.delete('/:employee_id', deleteEmployeeByIDRoutes);

module.exports = router;
