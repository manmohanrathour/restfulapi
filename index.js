/* eslint-disable no-console */
const express = require('express');
const logger = require('./src/middleware/logging');
const errorHandler = require('./src/middleware/errorHandler');

const app = express();

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Listening at port ${port}`);
});

const employees = require('./src/routes/employee.js');
const title = require('./src/routes/title.js');

app.use(logger.log);

app.use(express.json());
app.use('/employees', employees, errorHandler);
app.use('/title', title, errorHandler);
app.use('*', (req, res) => res.status(404).send('invalid url'));
app.use(logger.error);
